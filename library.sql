/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : localhost:3306
 Source Schema         : library

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 12/07/2020 18:11:27
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for books
-- ----------------------------
DROP TABLE IF EXISTS `books`;
CREATE TABLE `books`  (
  `id` int(11) NOT NULL COMMENT '图书编号',
  `title` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '图书名称',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '作者姓名',
  `publisher` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '出版社',
  `time` date NULL DEFAULT NULL COMMENT '出版时间',
  `price` decimal(7, 4) NULL DEFAULT NULL COMMENT '价格',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of books
-- ----------------------------
INSERT INTO `books` VALUES (10, '追梦之旅', '金昊霖', '追梦出版社', '2020-07-12', 66.0000);
INSERT INTO `books` VALUES (11, '博客计划', '金昊霖', '追梦出版社', '2020-07-01', 52.0000);
INSERT INTO `books` VALUES (12, '狼王梦 ', '沈石溪', '1902出版社', '2020-07-06', 36.0000);
INSERT INTO `books` VALUES (13, '实验报告', '金昊霖', '追梦出版社', '2020-06-22', 99.0000);

SET FOREIGN_KEY_CHECKS = 1;
