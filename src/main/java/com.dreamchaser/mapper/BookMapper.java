package com.dreamchaser.mapper;

import com.dreamchaser.domain.Book;

import java.util.List;
import java.util.Map;

public interface BookMapper {
    public List<Book> selectSmaller(Map<String,Object> map);
    public List<Book> selectAll();
    public List<Book> selectBigger(Map<String,Object> map);
    public List<Book> findBookByCondition(Map<String,Object> map);
    public List<Book> findBooksById(int id);
    public void insertBook(Map<String,Object> map);
    public void insertBooks(List<Book> books);
    public void updateBookById(Map<String,Object> map);
    public void updateBooks(List<Map<String,Object>> list);
    public void deleteBookById(int id);
    public void deleteBooksByIds(List<Integer> ids);
    /**
     * @param map 要注意这里的map中只能有一个对象
     * @return
     */
    public List<Book> findBookByConditionO(Map<String,Object> map);

}
