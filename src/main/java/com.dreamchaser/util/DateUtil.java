package com.dreamchaser.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 日期工具类，用于date和字符串之间的转换
 * @author 金昊霖
 */
public class DateUtil {
    public static String dateToString(Date date,String format){
        return new SimpleDateFormat(format).format(date);
    }
    public static Date stringToDate(String date,String format){
        try {
            return new SimpleDateFormat(date).parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
