package com.dreamchaser.util;

import com.dreamchaser.domain.Book;
import java.util.List;

/**
 * 显示类，用于展示传入的数据
 *
 * @author 金昊霖
 */
public class Displayer {
    public static void show(List<Book> list) {
        System.out.println("--------------------------------------------------------------------------");
        System.out.println("| 图书编号    书名           作者名     出版单位       出版时间            价格  |");
        for (Book book : list) {
            String date="";
            if (book.getTime()!=null){
                date=DateUtil.dateToString(book.getTime(),"yyyy-MM-dd");
            }
            System.out.println("| " + book.getId() + "       " + book.getTitle() + "         " + book.getName() + "     " +
                    book.getPublisher() + "     "+date+ "      " + book.getPrice() + " |");
        }
        System.out.println("--------------------------------------------------------------------------");
    }
    public static void show(Book book) {
        System.out.println("-------------------------------------------------------------------------");
        System.out.println("| 图书编号    书名           作者名     出版单位       出版时间            价格 |");


        String date="";
        if (book.getTime()!=null){
            date=DateUtil.dateToString(book.getTime(),"yyyy-MM-dd");
        }
        System.out.println("| " + book.getId() + "       " + book.getTitle() + " " + book.getName() + "     " +
                book.getPublisher() + "     "+date+ "      " + book.getPrice() + " |");

        System.out.println("-------------------------------------------------------------------------");
    }
}
