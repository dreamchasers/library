package com.dreamchaser;


import java.util.Scanner;

/**
 * 驱动类
 * 主要负责统一Sql和NoSql两个模式
 * 为了便捷，功能之间采用“套娃”的方式连接（实质就是避免递归调用造成堆栈溢出）
 * 套娃的基本思路：每个函数都有一个整数类型的返回值，只要返回1，就能返回上一级(实现原理，由该函数（记为函数1）的
 * 上一级（记为函数2）控制，如果函数1返回一了，就在函数2的上一级（记为函数3）再次调用函数2，即可做到返回上一层；
 * 而如果函数1返回0则在函数2再次循环调用，直至函数返回1)
 * 这样做不仅能实现功能，而且能避免多次“套娃”导致堆栈溢出的风险
 * @author 金昊霖
 */
public class Driver {

    static Scanner scanner = new Scanner(System.in);

    public  static void main(String[] args) {

        while (true) {
            if (choosePattern(1) == 1) {
                break;
            }
        }
    }
    /**
     * 一级模块
     * 选择模式模块
     * i 用于递归时判断其是否是第一次来还是输入错误来的
     *
     * @return 用于判断函数状态，0表示继续呆在这层，1表示退回上一层
     */
    private  static int choosePattern(int i) {
        if (i == 1) {
            System.out.println("\n\n\n||||图书信息管理系统||||       \n");
            System.out.println("技术栈选择：mysql+mybatis+java");
            System.out.println("作者：软工1902 金昊霖\n");
            System.out.println("请选择存储模式：");
            System.out.println("1.mysql存储（持久化规范数据存储）");
            System.out.println("2.简单运存存储（如要数据持久化，则需导出文件）");
            System.out.println("3.退出该系统\n\n");
            System.out.println("请输入你的选择（序号）：");
        }

        switch (scanner.nextInt()) {
            case 1:
                //这样做既能使其能返回上一级，而且想留在此级时不会造成“无限套娃”，即堆栈不断压缩的情况
                while (true) {
                    if (PatternSql.chooseFunction(1) == 1) {
                        return 0;
                    }
                }
            case 2:
                while (true) {
                    if (PatternNoSql.chooseFunction(1) == 1) {
                        return 0;
                    }
                }
            case 3:
                return 1;
            default:
                System.out.println("抱歉，输入的是非法字符，请重新输入：");
                //这里是采用递归，暂时没办法，如不采用会很麻烦
                return choosePattern(0);
        }
    }
}
