package com.dreamchaser;

import com.dreamchaser.mapper.BookMapper;
import com.dreamchaser.domain.Book;
import com.dreamchaser.util.DateUtil;
import com.dreamchaser.util.Displayer;
import com.dreamchaser.util.FileUtil;
import com.dreamchaser.util.SqlSessionUtil;
import org.apache.ibatis.session.SqlSession;

import java.math.BigDecimal;
import java.util.*;

/**
 * Sql模式
 */
public class PatternSql {
    static Scanner scanner = new Scanner(System.in);
    /**
     * mybatis中的sql
     */
    static SqlSession sqlSession = new SqlSessionUtil().getSqlSession();
    /**
     * 获取mybatis为我们创建好的Mapper对象
     */
    static BookMapper bookMapper = sqlSession.getMapper(BookMapper.class);
    /**
     * 空map，用于记录数据,每次方法加载时都要清空
     */
    static Map<String, Object> map = new HashMap<>();
    /**
     * 测试xml到底有没有载入
     * 这里就不删了，留作纪念
     * @param args
     */
//    public static void main(String[] args) {
////        PatternSql patternSql=new PatternSql();
//        SqlSession sqlSession =new SqlSessionUtil().getSqlSession();
//        List list = sqlSession.selectList("com.dreamchaser.mapper.BookMapper.selectAll");
//        Displayer.show(list);
//    }

    /**
     * 二级模块
     *
     * @param i 用于递归时判断其是否是第一次来还是输入错误来的
     * @return 用于返回上一级, 返回1就表示需要返回上一级，返回0则说明继续执行
     */
    public static int chooseFunction(int i) {
        if (i == 1) {
            System.out.println("\n\n功能：");
            System.out.println("1.图书信息录入");
            System.out.println("2.图书信息浏览");
            System.out.println("3.插入图书信息");
            System.out.println("4.查询");
            System.out.println("5.排序");
            System.out.println("6.修改图书信息");
            System.out.println("7.删除图书信息");
            System.out.println("8.导出为文件");
            System.out.println("9.返回模式选择");
            System.out.println("\n\n请输入你需要选择的功能（序号）：");
        }

        while (true) {
            switch (scanner.nextInt()) {
                case 1:
                    saveFile();
                    return 0;
                case 2:
                    selectAll();
                    return 0;
                case 3:
                    addOneBook();
                    return 0;
                case 4:
                    while (true) {
                        if (chooseSelect(1) == 1) {
                            return 0;
                        }
                    }
                case 5:
                    while (true) {
                        if (chooseOrder(1) == 1) {
                            return 0;
                        }
                    }
                case 6:
                    updateBook();
                    return 0;
                case 7:
                    deleteOne();
                    return 0;
                case 8:
                    writeToFile();
                    return 0;
                case 9:
                    return 1;
                default:
                    System.out.println("抱歉，输入的是非法字符，请重新输入：");
                    return chooseFunction(0);
            }
        }
    }

    private static void deleteOne() {
        System.out.println("请输入你要删除书籍的图书编号：");
        bookMapper.deleteBookById(scanner.nextInt());
        System.out.println("删除成功！");
    }

    private static void updateBook() {
        map.clear();
        System.out.println("请输入你要更新的书籍图书编号：");
        map.put("id", scanner.nextInt());
        scanner.nextLine();
        System.out.println("是否要修改其图书名称（若是，请输入其名称，若否，则输入“否”）：");
        String title = scanner.nextLine();
        if (!title.equals("否")) {
            map.put("title", title);
        }
        System.out.println("是否要修改其作者姓名（若是，请输入其姓名，若否，则输入“否”）：");
        String name = scanner.nextLine();
        if (!name.equals("否")) {
            map.put("name", name);
        }
        System.out.println("是否要修改其出版社（若是，请输入其出版社名称，若否，则输入“否”）：");
        String publisher = scanner.nextLine();
        if (!publisher.equals("否")) {
            map.put("publisher", publisher);
        }
        System.out.println("是否要修改其出版时间（若是，请输入其时间，请以“2020/10/06”的形式输入时间，若否，则输入“否”）：");
        String time = scanner.nextLine();
        if (!time.equals("否")) {
            map.put("time", new Date(time));
        }
        System.out.println("是否要修改其价格（若是，请输入其价格，若否，则输入“否”）：");
        String price = scanner.nextLine();
        if (!price.equals("否")) {
            map.put("price", new BigDecimal(price));
        }
        bookMapper.updateBookById(map);
        System.out.println("更新成功！");
    }
    private static void writeToFile() {
        System.out.println("请输入你要保存的文件名（如：dreamchaser.txt）：");
        scanner.nextLine();
        Boolean flag=FileUtil.writeFile(bookMapper.selectAll(),scanner.nextLine());
        if (flag){
            System.out.println("保存成功！");
        }else {
            System.out.println("保存失败，请确认输入的文件路径是否正确！");
        }

    }

    private static void saveFile() {
        System.out.println("请输入要录入的文件（txt，且需每一行都是一条记录）路径（绝对路径或者相对路径皆可）：");
        //把回车吃掉
        scanner.nextLine();
        List<String> list = FileUtil.readTxtFile(scanner.nextLine());
        String[] strings=null;
        if (list!=null){
            for (String s:list){
                strings=s.split(" |\n");
                map.clear();
                map.put("id",strings[0]);
                map.put("title",strings[1]);
                map.put("name",strings[2]);
                map.put("publisher",strings[3]);
                map.put("time", DateUtil.stringToDate(strings[4],"yyyy/MM/dd"));
                map.put("price",new BigDecimal(strings[5]));
                bookMapper.insertBook(map);
            }
            System.out.println("录入成功");
        }else {
            System.out.println("文件未找到或者文件不符合要求。请重新输入！");
        }

    }

    private static void selectAll() {
        List<Book> books = bookMapper.selectAll();
        Displayer.show(books);
    }

    private static void addOneBook() {
        map.clear();
        System.out.println("请输入你要插入书籍的图书编号：");
        map.put("id", scanner.nextInt());
        scanner.nextLine();
        System.out.println("请输入你要插入书籍的图书名称：");
        String title = scanner.nextLine();
        map.put("title", title);
        System.out.println("请输入你要插入书籍的作者姓名：");
        String name = scanner.nextLine();
        map.put("name", name);
        System.out.println("请输入你要插入书籍的出版社（若是，请输入其出版社名称，若否，则输入“否”）：");
        String publisher = scanner.nextLine();
        if (!publisher.equals("否")) {
            map.put("publisher", publisher);
        }
        System.out.println("请输入你要插入书籍的出版时间（若是，请输入其时间，请以“2020/10/06”的形式输入时间，若否，则输入“否”）：");
        String time = scanner.nextLine();
        if (!time.equals("否")) {
            map.put("time", new Date(time));
        }
        System.out.println("请输入你要插入书籍的价格（若是，请输入其价格，若否，则输入“否”）：");
        String price = scanner.nextLine();
        if (!price.equals("否")) {
            map.put("price", new BigDecimal(price));
        }
        bookMapper.insertBook(map);
        System.out.println("插入成功");
    }

    /**
     * 排序模块
     * 三级模块
     *
     * @param i
     * @return
     */
    private static int chooseOrder(int i) {
        if (i == 1) {
            System.out.println("\n\n排序：");
            System.out.println("1.按图书编号排序");
            System.out.println("2.按书名排序");
            System.out.println("3.按出版时间排序");
            System.out.println("4.按价格排序等");
            System.out.println("5.返回功能选择");
            System.out.println("\n\n请输入你需要的排序方式（序号）：");
        }

        while (true) {
            switch (scanner.nextInt()) {
                case 1:
                    selectOrderById();
                    return 0;
                case 2:
                    selectOrderByTitle();
                    return 0;
                case 3:
                    selectOrderByTime();
                    return 0;
                case 4:
                    selectOrderByPrice();
                    return 0;
                case 5:
                    return 1;
                default:
                    System.out.println("抱歉，输入的是非法字符，请重新输入：");
                    return chooseFunction(0);
            }
        }
    }

    private static void selectOrderByPrice() {
        map.clear();
        map.put("price", "1");
        List<Book> books = bookMapper.findBookByConditionO(map);
        Displayer.show(books);
    }

    private static void selectOrderByTime() {
        map.clear();
        map.put("time", "1");
        List<Book> books = bookMapper.findBookByConditionO(map);
        Displayer.show(books);
    }

    private static void selectOrderByTitle() {
        map.clear();
        map.put("title", "1");
        List<Book> books = bookMapper.findBookByConditionO(map);
        Displayer.show(books);
    }

    private static void selectOrderById() {
        map.clear();
        map.put("id", "1");
        List<Book> books = bookMapper.findBookByConditionO(map);
        Displayer.show(books);
    }

    /**
     * 查询模块
     * 三级模块
     *
     * @param i
     * @return
     */
    private static int chooseSelect(int i) {
        if (i == 1) {
            System.out.println("\n\n查询：");
            System.out.println("1.按书名查询");
            System.out.println("2.按作者名查询");
            System.out.println("3.按价格查询(小于)");
            System.out.println("4.按价格查询（等于）");
            System.out.println("5.按价格查询（大于）");
            System.out.println("6.返回模式选择");
            System.out.println("\n\n请输入你需要的查询方式（序号）：");
        }

        while (true) {
            switch (scanner.nextInt()) {
                case 1:
                    selectByTitle();
                    return 0;
                case 2:
                    selectByName();
                    return 0;
                case 3:
                    selectByPriceS();
                    return 0;
                case 4:
                    selectByPrice();
                    return 0;
                case 5:
                    selectByPriceB();
                    return 0;
                case 6:
                    return 1;
                default:
                    System.out.println("抱歉，输入的是非法字符，请重新输入：");
                    return chooseFunction(0);
            }
        }
    }

    private static void selectByPriceB() {
        System.out.println("请输入你要查询的价格：");
        map.clear();
        map.put("price", scanner.nextInt());
        List<Book> books = bookMapper.selectBigger(map);
        Displayer.show(books);
    }

    private static void selectByPrice() {
        System.out.println("请输入你要查询的价格：");
        map.clear();
        map.put("price", scanner.nextInt());
        List<Book> books = bookMapper.findBookByCondition(map);
        Displayer.show(books);
    }

    private static void selectByPriceS() {
        System.out.println("请输入你要查询的价格：");
        map.clear();
        map.put("price", new BigDecimal(scanner.nextInt()));
        List<Book> books = bookMapper.selectSmaller(map);
        Displayer.show(books);
    }

    private static void selectByName() {
        System.out.println("请输入你要查询的作者姓名：");
        map.clear();
        //因为后面是nextLine,而之前是nextInt，会算上回车键，所以得先用nextLine吃掉回车
        scanner.nextLine();
        map.put("name", scanner.nextLine());
        List<Book> books = bookMapper.findBookByCondition(map);
        Displayer.show(books);
    }

    private static void selectByTitle() {
        System.out.println("请输入你要查询的书籍名称：");
        map.clear();
        scanner.nextLine();
        map.put("title", scanner.nextLine());
        List<Book> books = bookMapper.findBookByCondition(map);
        Displayer.show(books);
    }

}
